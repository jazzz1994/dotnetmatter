﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerDemoEvents
{
    public delegate void EventHandler(customer c);
    public delegate DataTable ShowData();
    
    class Program:EventArgs
    {
        public static event EventHandler _Insert;
        public static event EventHandler _Update;
        public static event ShowData _Select;
        static void Main(string[] args)
        {
            _Insert += new EventHandler(InsertCustomer);
            _Update += new EventHandler(UpdateCustomer);
            _Select += new ShowData(ShowCustomer);

            customer c=new customer();
            c.CustID = 2;
            c.CustName = "abc";
            c.Dname = DeptName.IT.ToString();


            DataTable dt=_Select.Invoke();

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                object[] obj = dt.Rows[i].ItemArray;
                foreach (var item in obj)
                {
                    Console.Write(item + "\t");
                }
                Console.WriteLine();
            }

            //_Insert.Invoke(c);
           // _Update.Invoke(c);
            Console.Read();

            
        }
        public static DataTable ShowCustomer() 
        {
            SqlConnection cn = new SqlConnection("Data Source = SULAKSHANA_DELL; Initial Catalog = Demo1; Integrated Security = True;");
            SqlDataAdapter da = new SqlDataAdapter("select * from customer", cn);
            DataSet ds = new DataSet();

            da.Fill(ds, "customer");
            return ds.Tables[0];
        
        }

        public static  void InsertCustomer(customer c)
        {
            SqlConnection cn = new SqlConnection("Data Source = SULAKSHANA_DELL; Initial Catalog = Demo1; Integrated Security = True;");
            SqlDataAdapter da = new SqlDataAdapter("select * from customer", cn);
            DataSet ds = new DataSet();

            da.Fill(ds, "customer");
            DataRow drow = ds.Tables[0].NewRow();
            drow[0] = c.CustID;
            drow[1] = c.CustName;
            drow[2] = c.Dname;
            ds.Tables[0].Rows.Add(drow);
            SqlCommandBuilder bldr = new SqlCommandBuilder(da);
            da.Update(ds.Tables[0]);
            Console.Write("Inserted data successfully");

        }

        public static void UpdateCustomer(customer c)
        {
            SqlConnection cn = new SqlConnection("Data Source = SULAKSHANA_DELL; Initial Catalog = Demo1; Integrated Security = True;");
            SqlDataAdapter da = new SqlDataAdapter("select * from customer", cn);
            DataSet ds = new DataSet();
            da.MissingSchemaAction=MissingSchemaAction.AddWithKey;
            da.Fill(ds, "customer");
            DataRow drow=ds.Tables[0].Rows.Find(c.CustID);
            drow[1] = "pqr";
           
            SqlCommandBuilder bldr = new SqlCommandBuilder(da);
            da.Update(ds.Tables[0]);
            Console.Write("Updated data successfully");


        }
    }
}
