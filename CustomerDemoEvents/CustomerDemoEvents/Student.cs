﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CustomerDemoEvents
{
    
    public  delegate void InsertStudent(Student s);
    public delegate void DeleteStudent(Student s);

    public class Student
    {
        static List<Student> studList = new List<Student>();    
        
            public int Rollno { get; set; }
        public string StudentName { get; set; }
        public int Grade { get; set; }


        public void NewStudent(Student s)
        {
            studList.Add(s);
            Console.WriteLine("Student added successfully");

            foreach (var item in studList)
            {
                Console.WriteLine(item.Rollno);
                Console.WriteLine(item.StudentName);
                Console.WriteLine(item.Grade);
            }
            Console.WriteLine("------------");
        }

       public void RemoveStudent(Student s)
        {
            bool ans=studList.Contains(s);
            if (ans)
            {
                studList.Remove(s);
            }
            Console.WriteLine("Student remvoed successfully");
            Console.WriteLine("After removal");
            foreach (var item in studList)
            {
                Console.WriteLine(item.Rollno);
                Console.WriteLine(item.StudentName);
                Console.WriteLine(item.Grade);
            }
        }

    }
}
