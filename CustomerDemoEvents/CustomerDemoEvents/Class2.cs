﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerDemoEvents
{
    public delegate void Calculation(int i, int j);
    public delegate int showSquareRoot(int i);
    class MyClass
    {
     public  int CalSquareRoot(int i) {
            int ans=(int)Math.Sqrt(Convert.ToDouble(i));
            return ans;

        }

        public void Add(int no1, int no2)
        {
            Console.WriteLine(no1 + no2);
        }


        public void Subtraction(int no1, int no2)
        {
            Console.WriteLine(no1 - no2);
        }
    }

}
