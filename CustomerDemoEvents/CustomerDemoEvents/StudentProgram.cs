﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerDemoEvents
{
    class StudentProgram:EventArgs
    {
        public static event InsertStudent _AddStudent;
        public static event DeleteStudent _RemoveStudent;

        static void Main(string[] args)
        {


            _AddStudent += new InsertStudent(stud.NewStudent);

            _RemoveStudent += new CustomerDemoEvents.DeleteStudent(stud.RemoveStudent);

            Student stud = new Student();

            stud.Rollno = 1;
            stud.StudentName = "Abhishek";
            stud.Grade = 1;



            Student stud1= new Student();

            stud1.Rollno = 2;
            stud1.StudentName = "Bacchan";
            stud1.Grade = 2;

          
            

            _AddStudent(stud);
            _AddStudent(stud1);
            _RemoveStudent(stud);
            Console.Read();

        }
    }
}
